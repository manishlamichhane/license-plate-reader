from django.conf.urls import *

urlpatterns = [
    url(r'^v1/', include(('api.versions.v1.router', 'v1'), namespace='v1')),
]
