from django.conf.urls import include, url


urlpatterns = [
    url(r'^', include(('reader.api.v1.router', 'reader'), namespace='reader')),
]
