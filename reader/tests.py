from datetime import datetime
import os

from django.conf import settings
from django.test import client, TestCase
from django.urls import reverse

from rest_framework import status

from user.models import CustomUser
from owner import choices, models


class TestReader(TestCase):
    def setUp(self):
        user_data = {
            'email': 'john_kaglar@email.com',
            'date_of_birth': datetime.now()
        }
        self.user = CustomUser.objects.create(**user_data)

        owner_data = {
            'user': self.user,
            'salutation': choices.SALUTATION[0][0],
            'first_name': 'John',
            'last_name': 'Kaglar'
        }
        self.owner = models.VehicleOwner.objects.create(**owner_data)

        ownership_data = {
            'owner': self.owner,
            'license_type': choices.VEHICLE_TYPE[0][0],
            'vehicle_number': 'BA57PA1049'
        }
        self.ownership = models.Ownership.objects.create(**ownership_data)
        self.image_location = os.path.join(settings.POSTED_IMAGE_UPLOAD_DESTINATION, 'raw_image1.JPG')
        self.upload_url = reverse('api:v1:reader:reader-list')
        self.client = client.Client()

    def test_image_processing(self):
        with open(self.image_location, 'rb') as image:
            data = {
                'image': image
            }
            response = self.client.post(self.upload_url, data, format='multipart')

            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data['processed_text'], self.ownership.vehicle_number)
            self.assertEqual(response.data['owner']['full_name'], self.owner.full_name)
