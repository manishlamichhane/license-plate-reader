from rest_framework import routers

from reader.api.v1 import viewsets

app_name = 'reader'

router = routers.SimpleRouter()
router.register(r'reader', viewsets.Reader, base_name='reader')

urlpatterns = router.urls
