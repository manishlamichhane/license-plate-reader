from rest_framework import mixins, viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from utilities.automatic_license_plate_recognition import Alpr
from utilities import utils
from owner.models import Ownership


class Reader(mixins.CreateModelMixin, viewsets.GenericViewSet):
    renderer_classes = (JSONRenderer,)

    def create(self, request, *args, **kwargs):
        image = request.data.get("image")

        # create image in location defined in license/settings.py
        file_location = utils.handle_uploaded_file(image)

        # create image processing object
        alpr = Alpr(file_location)

        # create response dictionary
        response = {
            'processed_text': alpr.display_prediction()
        }

        # delete the created image
        utils.delete_image(file_location)

        # check and assign if owner with the vehicle number exists
        try:
            ownership = Ownership.objects.get(vehicle_number=response['processed_text'])
            response['owner'] = {
                'full_name': ownership.owner.full_name,
                'vehicle_type': ownership.get_license_type_display()
            }
        except Ownership.DoesNotExist:
            response['owner'] = 'Not Found'

        # return appropriate response
        return Response(response)
