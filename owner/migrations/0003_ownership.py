# Generated by Django 2.0 on 2018-03-13 18:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('owner', '0002_vehicleowner_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ownership',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('license_type', models.CharField(choices=[('A', 'Two Wheeler'), ('B', 'Four Wheeler Small'), ('C', 'Four Wheeler Medium'), ('D', 'Four Wheeler Large')], max_length=32)),
                ('vehicle_number', models.CharField(max_length=16)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='owner.VehicleOwner')),
            ],
        ),
    ]
