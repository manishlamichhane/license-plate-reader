from django.db import models

from user.models import CustomUser
from owner.choices import SALUTATION, VEHICLE_TYPE


class VehicleOwner(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    salutation = models.CharField(choices=SALUTATION, max_length=32)
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)

    @property
    def full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def __str__(self):
        return 'Vehicle Owner <{}>'.format(self.full_name)


class Ownership(models.Model):
    owner = models.ForeignKey(VehicleOwner, on_delete=models.CASCADE)
    license_type = models.CharField(choices=VEHICLE_TYPE, max_length=32)
    vehicle_number = models.CharField(max_length=16)

    def __str__(self):
        return 'Ownership <{} {} {}>'.format(self.owner, self.get_license_type_display(), self.vehicle_number)
