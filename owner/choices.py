SALUTATION = (
    ("Mr.", "MR."),
    ("Ms.", "MS."),
    ("Mrs.", "MRS."),
    ("Dr.", "DR.")
)

VEHICLE_TYPE = (
    ("A", "Two Wheeler"),
    ("B", "Four Wheeler Small"),
    ("C", "Four Wheeler Medium"),
    ("D", "Four Wheeler Large")
)
