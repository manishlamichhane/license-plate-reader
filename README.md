**INSTRUCTIONS**

- Step 1: Clone the repo and cd into it

Run: git clone `https://gitlab.com/manishlamichhane/license-plate-reader.git`
Run: cd <path/to/repo>


- Step 2: Create and Activate Virtual Environment

Run: python -m venv .venv (assuming python3 as system default)
Run: source .venv/bin/activate (for linux)
     path/to/.venv/Scripts/activate (for windows)

(.venv) appears infront of your current path in terminal

- Step 3: Install requirements

Run: pip install -r requirements.txt

This will install all project requirements

- Step 4: Run migrations

Skip this step. Currently, db.sqlite file is also committed which contains dummy data and has all tables migrated

- Step 5: Run the project:

Run: ./manage.py runserver

Now the project can be accessed at http://localhost:8000/api/v1/reader


- Step 6: Run test (OPTIONAL STEP):
Run: ./manage.py test reader.tests

This will run the tests in reader app