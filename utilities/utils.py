import os
import uuid

from django.conf import settings


def handle_uploaded_file(f):
    file_upload_path = get_unique_upload_path()
    with open(file_upload_path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return file_upload_path


def get_unique_upload_path():
    unique_name = uuid.uuid4()  # generates a unique uuid
    with_extension = '{}{}'.format(unique_name, settings.POSTED_IMAGE_EXTENSION)  # generates a unique name as<uuid>.jpg
    upload_path = os.path.join(settings.POSTED_IMAGE_UPLOAD_DESTINATION, with_extension)
    return upload_path


def delete_image(image_location):
    return os.remove(image_location)
